from django import forms
from django.forms import widgets
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class UserSettings(LoginRequiredMixin, FormView):
    template_name = 'users/settings.html'
    form_class = UserForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Sus datos fueron actualizados')
        return redirect('/accounts/')


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(
            label="Contraseña",
            max_length=100,
            help_text="Ambas contraseñas deben ser iguales.",
            widget=widgets.PasswordInput)
    password2 = forms.CharField(
            max_length=100,
            label="Repetir Contraseña",
            widget=widgets.PasswordInput)

    class Meta:
        model = User
        fields = ('username','first_name', 'last_name', 'email')

    def clean_email(self):
        value = self.cleaned_data['email']

        if User.objects.filter(email__iexact=value).exists():
            raise forms.ValidationError('Ya existe una cuenta con ese email')

        return value

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['password'] != cleaned_data['password2']:
            raise forms.ValidationError('Ambas contraseñas no coinciden')
        return cleaned_data

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])

        if commit:
            user.save()
        return user


class RegistrationView(FormView):
    template_name = 'registration/registration.html'
    form_class = RegistrationForm
    success_url = '/accounts/login/'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
